package usantatecla.draughts.models;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class TurnTest {

    private Turn turn;

    @Before
    public void before() {
        this.turn = new Turn();
    }

    @Test
    public void testChangeTurnOneTime() {
        this.turn.change();
        assertThat(this.turn.getColor(), is(Color.BLACK));
    }

    @Test
    public void testChangeTurnTwoTimes() {
        this.turn.change();
        this.turn.change();
        assertThat(this.turn.getColor(), is(Color.WHITE));
    }

    @Test
    public void testGetColorTurn() {
        assertThat(this.turn.getColor(), is(Color.WHITE));
    }

    @Test
    public void testGetOppositeColorTurn() {
        assertThat(this.turn.getOppositeColor(), is(Color.BLACK));
    }

}
