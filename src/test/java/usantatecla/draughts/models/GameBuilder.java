package usantatecla.draughts.models;

public class GameBuilder {

    private Game game;
    private Board board;

    public GameBuilder() {
        this.board = new Board();
        this.game = new Game(this.board);
    }

    public GameBuilder rows(String... rows) {
        for (int i = 0; i < Coordinate.getDimension(); i++) {
            for (int j = 0; j < Coordinate.getDimension(); j++) {
                Piece piece = getPiece(rows[i].charAt(j));
                if (piece != null) {
                    this.board.put(new CoordinateBuilder().row(i).column(j).build(), piece);
                }
            }
        }
        return this;
    }


    private Piece getPiece(char color) {
        if('b'==color) {
            return new Pawn(Color.WHITE);
        } else if('B'==color){
            return new Draught(Color.WHITE);
        } else if ('n'==color) {
            return new Pawn(Color.BLACK);
        } else if ('N'==color) {
            return new Draught(Color.BLACK);
        }
        return null;
    }

    public Game build() {
        return this.game;
    }

}
