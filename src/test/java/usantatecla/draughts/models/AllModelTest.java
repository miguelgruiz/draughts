package usantatecla.draughts.models;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ 
    CoordinateTest.class,
    GameMovementTest.class,
    GameTest.class,
    PieceTest.class,
    StateTest.class,
    TurnTest.class } )
public class AllModelTest {
    
}