package usantatecla.draughts.models;

import java.util.Arrays;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

public class PieceTest {

    @Test
    public void testIsLimitWhitePawnIsTrue() {
        Coordinate coordinate = new CoordinateBuilder().row(0).column(1).build();
        assertThat(pawn(white()).isLimit(coordinate), is(true));
    }

    @Test
    public void testIsLimitWhitePawnIsFalse() {
        Coordinate coordinate = new CoordinateBuilder().row(2).column(1).build();
        assertThat(pawn(white()).isLimit(coordinate), is(false));
    }

    @Test
    public void testIsLimitBlackPawnIsTrue() {
        Coordinate coordinate = new CoordinateBuilder().row(7).column(0).build();
        assertThat(pawn(black()).isLimit(coordinate), is(true));
    }

    @Test
    public void testIsLimitBLackPawnIsFalse() {
        Coordinate coordinate = new CoordinateBuilder().row(6).column(1).build();
        assertThat(pawn(black()).isLimit(coordinate), is(false));
    }

    @Test
    public void testGetColor() {
        assertThat(pawn(white()).getColor(), is(Color.WHITE));
        assertThat(pawn(black()).getColor(), is(Color.BLACK));
    }

    @Test
    public void testGetCode() {
        assertThat(pawn(white()).getCode(), is("b"));
        assertThat(pawn(black()).getCode(), is("n"));
    }

    @Test
    public void testisAdvancedWhitePawnIsTrue() {
        Coordinate origin = new CoordinateBuilder().row(7).column(2).build();
        Coordinate target = new CoordinateBuilder().row(6).column(3).build();
        assertThat(pawn(white()).isAdvanced(origin, target), is(true));
    }

    @Test
    public void testisAdvancedWhitePawnIsFalse() {
        Coordinate origin = new CoordinateBuilder().row(6).column(3).build();
        Coordinate target = new CoordinateBuilder().row(7).column(2).build();
        assertThat(pawn(white()).isAdvanced(origin, target), is(false));
    }

    @Test
    public void testisAdvancedBlackPawnIsTrue() {
        Coordinate origin = new CoordinateBuilder().row(0).column(3).build();
        Coordinate target = new CoordinateBuilder().row(1).column(4).build();
        assertThat(pawn(black()).isAdvanced(origin, target), is(true));
    }

    @Test
    public void testisAdvancedBlackPawnIsFalse() {
        Coordinate origin = new CoordinateBuilder().row(1).column(4).build();
        Coordinate target = new CoordinateBuilder().row(0).column(3).build();
        assertThat(pawn(black()).isAdvanced(origin, target), is(false));
    }

    @Test
    public void testIsCorrectDiagonalMovementPawn() {
        Coordinate[] coordinates = new Coordinate[] {new CoordinateBuilder().row(7).column(2).build(), new CoordinateBuilder().row(6).column(3).build()};
        assertThat(pawn(white()).isCorrectDiagonalMovement(0,0,coordinates), is(nullValue()));
    }

    @Test
    public void testIsCorrectDiagonalMovementPawnErrorNotAdvanced() {
        Coordinate[] coordinates = new Coordinate[] {new CoordinateBuilder().row(6).column(3).build(), new CoordinateBuilder().row(7).column(2).build()};
        assertThat(pawn(white()).isCorrectDiagonalMovement(0,0,coordinates), is(Error.NOT_ADVANCED));
    }

    @Test
    public void testIsCorrectDiagonalMovementPawnErrorTwoMuchAdvanced() {
        Coordinate[] coordinates = new Coordinate[] {new CoordinateBuilder().row(7).column(2).build(), new CoordinateBuilder().row(4).column(5).build()};
        assertThat(pawn(white()).isCorrectDiagonalMovement(0,0,coordinates), is(Error.TOO_MUCH_ADVANCED));
    }

    @Test
    public void testIsCorrectDiagonalMovementPawnErrorWithoutEating() {
        Coordinate[] coordinates = new Coordinate[] {new CoordinateBuilder().row(7).column(2).build(), new CoordinateBuilder().row(5).column(4).build()};
        assertThat(pawn(white()).isCorrectDiagonalMovement(0,0,coordinates), is(Error.WITHOUT_EATING));
    }

    @Test
    public void testIsCorrectDiagonalMovementDraught() {
        Coordinate[] coordinates = new Coordinate[] {new CoordinateBuilder().row(7).column(2).build(), new CoordinateBuilder().row(6).column(3).build()};
        assertThat(draught(white()).isCorrectDiagonalMovement(0,0,coordinates), is(nullValue()));
    }

    @Test
    public void testIsCorrectDiagonalMovementDraughtErrorTooMuchEatings() {
        Coordinate[] coordinates = new Coordinate[] {new CoordinateBuilder().row(6).column(3).build(), new CoordinateBuilder().row(7).column(2).build()};
        assertThat(draught(white()).isCorrectDiagonalMovement(2,0,coordinates), is(Error.TOO_MUCH_EATINGS));
    }

    @Test
    public void testIsCorrectMovement() {
        Coordinate[] coordinates = new Coordinate[] {new CoordinateBuilder().row(7).column(2).build(), new CoordinateBuilder().row(5).column(4).build()};
        assertThat(pawn(white()).isCorrectMovement(Arrays.asList(pawn(black())),0,coordinates), is(nullValue()));
    }

    @Test
    public void testIsCorrectMovementErrorNotDiagonal() {
        Coordinate[] coordinates = new Coordinate[] {new CoordinateBuilder().row(7).column(2).build(), new CoordinateBuilder().row(7).column(4).build()};
        assertThat(pawn(white()).isCorrectMovement(Arrays.asList(pawn(black())),0,coordinates), is(Error.NOT_DIAGONAL));
    }

    @Test
    public void testIsCorrectMovementErrorColleagueEating() {
        Coordinate[] coordinates = new Coordinate[] {new CoordinateBuilder().row(7).column(2).build(), new CoordinateBuilder().row(5).column(4).build()};
        assertThat(pawn(white()).isCorrectMovement(Arrays.asList(pawn(white())),0,coordinates), is(Error.COLLEAGUE_EATING));
    }

    private static Pawn pawn(Color color) {
        return new Pawn(color);
    }

    private static Draught draught(Color color) {
        return new Draught(color);
    }

    private static Color white() {
        return Color.WHITE;
    }

    private static Color black() {
        return Color.BLACK;
    }

}
