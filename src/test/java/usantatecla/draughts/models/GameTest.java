package usantatecla.draughts.models;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

public class GameTest {

    private Game game;

    @Before
    public void before() {
        this.game = new GameBuilder().rows(
                " n n n n",
                "n n n n ",
                " n n n n",
                "        ",
                "        ",
                "b b b b ",
                " b b b b",
                "b b b b ").build();
    }

    @Test
    public void testGetColorWhitePawnCoordinateIsWhite() {
        assertThat(this.game.getColor(new CoordinateBuilder().row(7).column(0).build()), is(Color.WHITE));
    }

    @Test
    public void testGetColorWhitePawnCoordinateIsBlack() {
        assertThat(this.game.getColor(new CoordinateBuilder().row(0).column(1).build()), is(Color.BLACK));
    }

    @Test
    public void testGetTurnColorWhiteTurnIsWhite() {
        assertThat(this.game.getTurnColor(), is(Color.WHITE));
    }

    @Test
    public void testGetPieceCoordinateExitsPiece() {
        assertThat(this.game.getPiece(new CoordinateBuilder().row(7).column(0).build()), instanceOf(Piece.class));
    }

    @Test
    public void testGetPieceCoordinateNotExitsPiece() {
        assertThat(this.game.getPiece(new CoordinateBuilder().row(7).column(1).build()), is(nullValue()));
    }



}
