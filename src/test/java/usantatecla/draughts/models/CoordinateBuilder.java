package usantatecla.draughts.models;

public class CoordinateBuilder {

    private int row;
    private int column;

    public CoordinateBuilder() {
        row = 0;
        column = 0;
    }

    public CoordinateBuilder row(int row) {
        this.row = row;
        return this;
    }

    public CoordinateBuilder column(int column) {
        this.column = column;
        return this;
    }

    public Coordinate build() {
        return new Coordinate(row, column);
    }



}
