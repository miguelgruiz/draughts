package usantatecla.draughts.models;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;

public class CoordinateTest {


    private Coordinate coordinate00;
    private Coordinate coordinate01;
    private Coordinate coordinate32;
    private Coordinate coordinate34;
    private Coordinate coordinate43;
    private Coordinate coordinate52;
    private Coordinate coordinate54;
    private Coordinate coordinate65;
    private Coordinate coordinate67;
    private Coordinate coordinate76;

    @Before
    public void before() {
        this.coordinate00 = new CoordinateBuilder().row(0).column(0).build();
        this.coordinate01 = new CoordinateBuilder().row(0).column(1).build();
        this.coordinate32 = new CoordinateBuilder().row(3).column(2).build();
        this.coordinate34 = new CoordinateBuilder().row(3).column(4).build();
        this.coordinate43 = new CoordinateBuilder().row(4).column(3).build();
        this.coordinate52 = new CoordinateBuilder().row(5).column(2).build();
        this.coordinate54 = new CoordinateBuilder().row(5).column(4).build();
        this.coordinate65 = new CoordinateBuilder().row(6).column(5).build();
        this.coordinate67 = new CoordinateBuilder().row(6).column(7).build();
        this.coordinate76 = new CoordinateBuilder().row(7).column(6).build();
    }

    @Test
    public void testGetDirectionsTwoCoordinatesAreInTheSameDiagonal() {
        assertThat(coordinate43.getDirection(coordinate54), is(Direction.NE));
        assertThat(coordinate43.getDirection(coordinate34), is(Direction.SE));
        assertThat(coordinate43.getDirection(coordinate32), is(Direction.SW));
        assertThat(coordinate43.getDirection(coordinate52), is(Direction.NW));
    }

    @Test
    public void testGetDirectionsTwoCoordinatesNotAreInTheSameDiagonal() {
        assertThat(coordinate32.getDirection(coordinate34), is(nullValue()));
    }

    @Test
    public void testGetDiagonalDistanceTwoCoordinatesAreInTheSameDiagonal() {
        assertThat(coordinate32.getDiagonalDistance(coordinate43), is(1));
        assertThat(coordinate32.getDiagonalDistance(coordinate54), is(2));
    }

    @Test(expected=AssertionError.class)
    public void testGetDiagonalDistanceTwoCoordinatesAreNotInTheSameDiagonal() {
        coordinate32.getDiagonalDistance(coordinate34);
    }


    @Test
    public void testGetBetweenDiagonalCoordinatesTwoCoordinatesAreInTheSameDiagonal() {
        List<Coordinate> expectedCoordinates = new ArrayList<>();
        expectedCoordinates.add(coordinate43);
        List<Coordinate> resultCoordinates = coordinate32.getBetweenDiagonalCoordinates(coordinate54);
        assertThat(resultCoordinates, is(expectedCoordinates));
    }

    @Test(expected=AssertionError.class)
    public void testGetBetweenDiagonalCoordinatesTwoCoordinatesAreNotInTheSameDiagonal() {
        coordinate32.getBetweenDiagonalCoordinates(coordinate34);
    }

    @Test
    public void testGetDiagonalCoordinatesCoordinateOneLevel() {
        List<Coordinate> expectedCoordinates = new ArrayList<>();
        expectedCoordinates.add(coordinate54);
        expectedCoordinates.add(coordinate34);
        expectedCoordinates.add(coordinate32);
        expectedCoordinates.add(coordinate52);
        List<Coordinate> resultCoordinates = coordinate43.getDiagonalCoordinates(1);
        assertThat(resultCoordinates, is(expectedCoordinates));
    }

    @Test
    public void testGetDiagonalCoordinatesCoordinateOneLevelInTheBorder() {
        List<Coordinate> expectedCoordinates = new ArrayList<>();
        expectedCoordinates.add(coordinate67);
        expectedCoordinates.add(coordinate65);
        List<Coordinate> resultCoordinates = coordinate76.getDiagonalCoordinates(1);
        assertThat(resultCoordinates, is(expectedCoordinates));
    }

    @Test
    public void testIsBlackCoordinateIsTrue() {
        assertThat(coordinate01.isBlack(), is(true));
    }

    @Test
    public void testIsBlackCoordinateIsFalse() {
        assertThat(coordinate00.isBlack(), is(false));
    }

    @Test
    public void testIsLastCoordinateIsTrue() {
        assertThat(coordinate76.isLast(), is(true));
    }

    @Test
    public void testIsLastCoordinateIsFalse() {
        assertThat(coordinate43.isLast(), is(false));
    }

    @Test
    public void testIsFirstCoordinateIsTrue() {
        assertThat(coordinate01.isFirst(), is(true));
    }

    @Test
    public void testIsFirstCoordinateIsFalse() {
        assertThat(coordinate43.isFirst(), is(false));
    }

    @Test
    public void testGetRowCoordinate() {
        assertThat(coordinate43.getRow(), is(4));
    }

    @Test
    public void testGetColumnCoordinate() {
        assertThat(coordinate43.getColumn(), is(3));
    }

}
