package usantatecla.draughts.models;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

public class GameMovementTest {

    private Game game;

    @Test
    public void testResetWithStartGame() {
        this.game = new GameBuilder().rows(
                " n n n n",
                "n n n n ",
                " n n n n",
                "        ",
                " b      ",
                "  b b b ",
                " b b b b",
                "b b b b ").build();
        Game resetGame = new GameBuilder().rows(
                " n n n n",
                "n n n n ",
                " n n n n",
                "        ",
                "        ",
                "b b b b ",
                " b b b b",
                "b b b b ").build();
        this.game.reset();
        assertThat(this.game, is(resetGame));
    }

    @Test
    public void testMove() {
        this.game = new GameBuilder().rows(
                " n n n n",
                "n n n n ",
                " n n n n",
                "        ",
                " b      ",
                "  b b b ",
                " b b b b",
                "b b b b ").build();
        Coordinate[] coordinates = new Coordinate[] {new CoordinateBuilder().row(5).column(2).build(), new CoordinateBuilder().row(4).column(3).build()};
        assertThat(this.game.move(coordinates), is(nullValue()));
    }

    @Test
    public void testMoveErrorEmptyOrigin() {
        this.game = new GameBuilder().rows(
                " n n n n",
                "n n n n ",
                " n n n n",
                "        ",
                " b      ",
                "  b b b ",
                " b b b b",
                "b b b b ").build();
        Coordinate[] coordinates = new Coordinate[] {new CoordinateBuilder().row(5).column(0).build(), new CoordinateBuilder().row(4).column(1).build()};
        assertThat(this.game.move(coordinates), is(Error.EMPTY_ORIGIN));
    }

    @Test
    public void testMoveErrorOppositePiece() {
        this.game = new GameBuilder().rows(
                " n n n n",
                "n n n n ",
                " n n n n",
                "        ",
                " b      ",
                "  b b b ",
                " b b b b",
                "b b b b ").build();
        Coordinate[] coordinates = new Coordinate[] {new CoordinateBuilder().row(2).column(1).build(), new CoordinateBuilder().row(3).column(0).build()};
        assertThat(this.game.move(coordinates), is(Error.OPPOSITE_PIECE));
    }

    @Test
    public void testMoveErrorNotEmptyTarget() {
        this.game = new GameBuilder().rows(
                " n n n n",
                "n n n n ",
                " n n n n",
                "        ",
                " b      ",
                "  b b b ",
                " b b b b",
                "b b b b ").build();
        Coordinate[] coordinates = new Coordinate[] {new CoordinateBuilder().row(5).column(2).build(), new CoordinateBuilder().row(4).column(1).build()};
        assertThat(this.game.move(coordinates), is(Error.NOT_EMPTY_TARGET));
    }

    @Test
    public void testMoveErrorTooMuchJumps() {
        this.game = new GameBuilder().rows(
                "        ",
                "        ",
                "     n  ",
                "        ",
                "   n    ",
                "        ",
                " n      ",
                "b       ").build();
        Coordinate[] coordinates = new Coordinate[] {
                new CoordinateBuilder().row(7).column(0).build(),
                new CoordinateBuilder().row(5).column(2).build(),
                new CoordinateBuilder().row(3).column(4).build(),
                new CoordinateBuilder().row(1).column(6).build(),
                new CoordinateBuilder().row(0).column(7).build()};
        assertThat(this.game.move(coordinates), is(Error.TOO_MUCH_JUMPS));
    }


    @Test
    public void testBlockedPawnIsTrue() {
        this.game = new GameBuilder().rows(
                "        ",
                "  n   n ",
                "   n n  ",
                "    b   ",
                "        ",
                "        ",
                "        ",
                "        ").build();
        assertThat(this.game.isBlocked(), is(true));
    }

    @Test
    public void testBlockedPawnIsFalse() {
        this.game = new GameBuilder().rows(
                "        ",
                "  n     ",
                "   n n  ",
                "    b   ",
                "        ",
                "        ",
                "        ",
                "        ").build();
        assertThat(this.game.isBlocked(), is(false));
    }

    @Test
    public void testBlockedDraugthIsTrue() {
        this.game = new GameBuilder().rows(
                "        ",
                "  n   n ",
                "   n n  ",
                "    B   ",
                "   n n  ",
                "  n   n ",
                "        ",
                "        ").build();
        assertThat(this.game.isBlocked(), is(true));
    }


    @Test
    public void testBlockedDraugthIsFalse() {
        this.game = new GameBuilder().rows(
                "        ",
                "  n   n ",
                "   n n  ",
                "    B   ",
                "   n n  ",
                "      n ",
                "        ",
                "        ").build();
        assertThat(this.game.isBlocked(), is(false));
    }





}
