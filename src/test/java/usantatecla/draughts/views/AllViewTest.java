package usantatecla.draughts.views;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    PlayViewTest.class,
    ResumeViewTest.class } )
public class AllViewTest {
    
}