package usantatecla.draughts.views;

import usantatecla.draughts.controllers.PlayController;
import usantatecla.draughts.models.Color;
import usantatecla.draughts.models.Coordinate;
import usantatecla.draughts.models.CoordinateBuilder;
import usantatecla.draughts.utils.Console;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PlayViewTest {

    @Mock
    Console console;

    @Mock
    PlayController playController;

    @InjectMocks
    PlayView playView = new PlayView();

    @Test
    public void testGivenNewCoordinateWhenReadCoordinateThenIsCorrect() {
        when(this.console.readString("Mueven las blancas: ")).thenReturn("61.52");
        when(this.playController.getColor()).thenReturn(Color.WHITE);
        this.playView.interact(playController);
        Coordinate origin = new CoordinateBuilder().row(5).column(0).build();
        Coordinate target = new CoordinateBuilder().row(4).column(1).build();
        verify(this.playController).move(origin,target);
    }

}
