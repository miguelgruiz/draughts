package usantatecla.draughts.views;

import usantatecla.draughts.controllers.PlayController;
import usantatecla.draughts.controllers.ResumeController;
import usantatecla.draughts.models.Color;
import usantatecla.draughts.models.Coordinate;
import usantatecla.draughts.models.CoordinateBuilder;
import usantatecla.draughts.utils.Console;
import usantatecla.draughts.utils.YesNoDialog;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ResumeViewTest {

    @Mock
    YesNoDialog yesNoDialog;

    @Mock
    ResumeController resumeController;

    @InjectMocks
    ResumeView resumeView = new ResumeView();

    @Test
    public void testGivenTrueYesNoDialogWhenReadFromConsoleThenResetGame() {
        when(this.yesNoDialog.read("¿Queréis jugar otra")).thenReturn(true);
        this.resumeView.interact(resumeController);
        verify(this.resumeController).reset();
    }

    @Test
    public void testGivenFalseYesNoDialogWhenReadFromConsoleThenNextState() {
        when(this.yesNoDialog.read("¿Queréis jugar otra")).thenReturn(false);
        this.resumeView.interact(resumeController);
        verify(this.resumeController).next();
    }
}
