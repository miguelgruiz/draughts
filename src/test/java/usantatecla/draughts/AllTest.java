package usantatecla.draughts;

import usantatecla.draughts.controllers.AllControllerTest;
import usantatecla.draughts.models.AllModelTest;
import usantatecla.draughts.views.AllViewTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        AllModelTest.class,
        AllViewTest.class,
        AllControllerTest.class} )
public class AllTest {
    
}