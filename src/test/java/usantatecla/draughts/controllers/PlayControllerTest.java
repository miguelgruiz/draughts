package usantatecla.draughts.controllers;

import usantatecla.draughts.models.Coordinate;
import usantatecla.draughts.models.CoordinateBuilder;
import usantatecla.draughts.models.Error;
import usantatecla.draughts.models.Game;
import usantatecla.draughts.models.GameBuilder;
import usantatecla.draughts.models.State;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

public class PlayControllerTest {

    private PlayController playController;

    @Before
    public void before() {
        this.playController = new PlayController(new Game(), new State());
    }

    @Test(expected=AssertionError.class)
    public void testMoveMinimumCoordinates() {
        Coordinate[] coordinates = new Coordinate[] {new CoordinateBuilder().row(5).column(2).build()};
        this.playController.move(coordinates);
    }

    @Test
    public void testMove() {
        Coordinate[] coordinates = new Coordinate[] {new CoordinateBuilder().row(5).column(2).build(), new CoordinateBuilder().row(4).column(3).build()};
        assertThat(this.playController.move(coordinates), is(nullValue()));
    }

    @Test
    public void testMoveEmptyOrigin() {
        Coordinate[] coordinates = new Coordinate[] {new CoordinateBuilder().row(4).column(1).build(), new CoordinateBuilder().row(3).column(2).build()};
        assertThat(this.playController.move(coordinates), is(Error.EMPTY_ORIGIN));
    }

    @Test
    public void testMoveOppositePiece() {
        Coordinate[] coordinates = new Coordinate[] {new CoordinateBuilder().row(2).column(1).build(), new CoordinateBuilder().row(3).column(0).build()};
        assertThat(this.playController.move(coordinates), is(Error.OPPOSITE_PIECE));
    }

    @Test
    public void testMoveNotEmptyTarget() {
        Coordinate[] coordinates = new Coordinate[]{new CoordinateBuilder().row(6).column(1).build(), new CoordinateBuilder().row(5).column(2).build()};
        assertThat(this.playController.move(coordinates), is(Error.NOT_EMPTY_TARGET));
    }

    @Test
    public void testMoveEmptyTooMuchJumps() {
        Game game = new GameBuilder().rows(
                "        ",
                "        ",
                "     n  ",
                "        ",
                "   n    ",
                "        ",
                " n      ",
                "b       ").build();
        this.playController = new PlayController(game, new State());
        Coordinate[] coordinates = new Coordinate[]{
                new CoordinateBuilder().row(7).column(0).build(),
                new CoordinateBuilder().row(5).column(2).build(),
                new CoordinateBuilder().row(3).column(4).build(),
                new CoordinateBuilder().row(1).column(6).build(),
                new CoordinateBuilder().row(0).column(7).build()};

        assertThat(this.playController.move(coordinates), is(Error.TOO_MUCH_JUMPS));

    }

    @Test
    public void testIsBlockedIsFalse() {
        assertThat(this.playController.isBlocked(), is(false));
    }

    @Test
    public void testIsBlockedIsTrue() {
        Game game = new GameBuilder().rows(
                "        ",
                "  n   n ",
                "   n n  ",
                "    b   ",
                "        ",
                "        ",
                "        ",
                "        ").build();
        this.playController = new PlayController(game, new State());
        assertThat(this.playController.isBlocked(), is(true));
    }

}
